from gensim.models import Word2Vec

import json

def convertImageGraphletToWords(graphlets):
	imageWordsList = list()
	for image_graphlet in graphlets:
		wordsForImage = list()
		for sens in [c['names'] for c in image_graphlet['annotations']['objects']]:
			for sen in sens:
				wordsForImage.append(sen);
		imageWordsList.append(wordsForImage)
	return imageWordsList
	
def convertSentenceGraphletToWords(graphlets):
	sentenceObjectsList = list()
	for graphlet in graphlets:
		sentenceObjectsList.append(graphlet['objects'])
	return sentenceObjectsList

"""
Collect all the data.
"""
f = open('data/sentences.json', 'r')
sentences = json.load(f)
f.close()

f = open('data/sentence_graphs.json', 'r')
sentence_graphlets = json.load(f)
f.close()

f = open('data/consolidated_graph_annotations.json', 'r')
image_graphlets = json.load(f)
f.close()


imageWords = convertImageGraphletToWords(image_graphlets)
sentenceWords = convertSentenceGraphletToWords(sentence_graphlets)

"""
Get word2vecModel
"""

s = sentenceWords		

for wordList in imageWords:
	for word in wordList:
		s.append(word)
			
word2vecModel = Word2Vec(s, min_count=1)

a1 = word2vecModel.similarity('building', 'house')
b1 = word2vecModel.similarity('towel', 'shower')

word2vecModel.save('data/GraphObjectsTrainedWord2vecModel.txt')
m = Word2Vec.load('data/GraphObjectsTrainedWord2vecModel.txt')

a2 = m.similarity('building', 'house')
b2 = m.similarity('towel', 'shower')

assert a1 == a2
assert b1 == b2
