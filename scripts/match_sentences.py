import json

def get_key(url):
  url = url[::-1]
  try:
    first = url.index('_')
    second = url[first+1:].index('_')
    return url[first+1:first+1+second][::-1]
  except ValueError:
    return None

val_sentence_file = open('data/val_collected.json', 'r')
train_sentence_file = open('data/train_collected.json', 'r')
val_sentences = json.load(val_sentence_file)
train_sentences = json.load(train_sentence_file)
sentences = val_sentences + train_sentences

val_sentence_file.close()
train_sentence_file.close()

graph_file = open('data/graph_annotations.json', 'r')
graphs = json.load(graph_file)

new_sentences = []
for index in range(len(sentences)):
  sentence = sentences[index]
  url = sentence['url']
  key = get_key(url)
  for image_id in range(len(graphs)):
    graph = graphs[image_id]
    if key in graph['url']:
      sentence['image_id'] = image_id
      sentence['pk'] = graph['pk']
      new_sentences.append(sentence)
      break
print json.dumps(new_sentences)
