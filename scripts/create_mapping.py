import json

f = open('data/matched_sentences.json', 'r')
matched_sentences = json.load(f)
f.close()

f = open('data/graph_annotations.json', 'r')
graphs = json.load(f)
f.close()

sentences = []
consolidated_graphs = []
sentence_to_image_mapping = []
image_to_sentence_mapping = []

intermediate_map = {}
global_id = 0

def get_image_id(image_id):
  global global_id
  if image_id in intermediate_map:
    return intermediate_map[image_id]
  graph = graphs[image_id]
  consolidated_graphs.append(graph)
  image_to_sentence_mapping.append([])
  intermediate_map[image_id] = global_id
  global_id += 1
  return global_id - 1

index = 0
for matched_sentence in matched_sentences:
  for sentence in matched_sentence['sentences']:
    image_id = get_image_id(matched_sentence['image_id'])
    sentences.append(sentence)
    sentence_to_image_mapping.append(image_id)
    image_to_sentence_mapping[image_id].append(index)
    index += 1

print len(sentences)
print len(sentence_to_image_mapping)
print len(image_to_sentence_mapping)
print len(consolidated_graphs)

f = open('data/sentences.json', 'w')
f.write(json.dumps(sentences))
f.close()

f = open('data/sentence_to_image_mapping.json', 'w')
f.write(json.dumps(sentence_to_image_mapping))
f.close()

f = open('data/image_to_sentence_mapping.json', 'w')
f.write(json.dumps(image_to_sentence_mapping))
f.close()

f = open('data/consolidated_graph_annotations.json', 'w')
f.write(json.dumps(consolidated_graphs))
f.close()
