from stanford_parser import Parser, jvm

import json

jvm.start()

f = open('data/sentences.json', 'r')
sentences = json.load(f)
f.close()

sentence_graphs = []
parser = Parser()

for sentence in sentences:
  print sentence
  sentence_graph = parser.parse(sentence)
  sentence_graphs.append(sentence_graph)

f = open('data/sentence_graphs.json', 'w')
f.write(json.dumps(sentence_graphs))
f.close()

jvm.close()
