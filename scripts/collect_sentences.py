"""
This script takes the training and validation sentence data structure from Microsoft's COCO
and accumulates all the sentences of a given image together so that it is easier to work with.

The new format will be the following:
[
  {
    url: "someurl.jpg"
    sentences: ['Sentence 1', 'Sentence 2'...]
  },
...
]
"""
import json
import sys

if len(sys.argv) < 3:
  print 'this script requires a json file import that contains the COCO sentence data as well as a output filename'
f = open(sys.argv[1], 'r')
data = json.load(f)
f.close()

id_map = {}

for img in data['images']:
  id_map[img['id']] = {'url': img['url'], 'sentences': []}

for sentence in data['sentences']:
  img = id_map[sentence['image_id']]
  img['sentences'].append(sentence['sentence'])

output = open(sys.argv[2], 'w')
json.dump(id_map.values(), output)
output.close()
