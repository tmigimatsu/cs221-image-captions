from collections import Counter
import json

"""
Collect all the data.
"""
f = open('data/sentence_graphs.json', 'r')
sentences = json.load(f)
f.close()

f = open('data/consolidated_graph_annotations.json', 'r')
graphs = json.load(f)
f.close()

f = open('data/sentence_to_image_mapping.json', 'r')
sentence_to_image_mapping = json.load(f)
f.close()

f = open('data/image_to_sentence_mapping.json', 'r')
image_to_sentence_mapping = json.load(f)
f.close()

def get_image_objects(graph):
  output = []
  for obj in graph['annotations']['objects']:
    for name in obj['names']:
      output.append(name)
  return output

def get_sentence_objects(sentence):
  return sentence['objects']

def check_sentence(id):
  output = []
  matched = []
  sentence = sentences[id]
  image = graphs[sentence_to_image_mapping[id]]
  sentence_objs = get_sentence_objects(sentence)
  image_objs = get_image_objects(image)
  for sentence_obj in sentence_objs:
    found = len(filter(lambda y: sentence_obj.lower() in y.lower(), image_objs))
    if not found:
      output.append(sentence_obj)
    else:
      matched.append(sentence_obj)
  return output, matched

def collect_map():
  unmatched_map = Counter()
  matched_map = Counter()
  for index in range(len(sentences)):
    unmatched, matched = check_sentence(index)
    for elem in unmatched:
      unmatched_map[elem] += 1
    for elem in matched:
      matched_map[elem] += 1
  return unmatched_map, matched_map

def common_sentence_objects():
  output = Counter()
  for sentence in sentences:
    sentence_objs = get_sentence_objects(sentence)
    for obj in sentence_objs:
      output[obj] += 1
  return output

def add_to_html(filename, counter):
  f = open(filename, 'w')
  f.write('<html><body><ul>\n')
  for elem in counter.most_common():
    f.write('<li>' + elem[0] + ': ' + str(elem[1]) + '</li>\n')
  f.write('</html></body></ul>\n')
  f.close()

def compare_percentage_in_top(num):
  unmatched, matched = collect_map()
  common = common_sentence_objects()
  count = 0
  index = 0
  output = []
  for elem in common.most_common():
    if index >= num:
      break
    index += 1
    if elem[0] in matched:
      count += 1
    else:
      output.append(elem)
  return float(count)/float(index), output
