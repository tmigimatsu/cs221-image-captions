"""
This script takes the consolidated graph annotations and simplifies them to
match the format of the sentence graphs.

The new format will be the following:
[
    {
        entities: ['object1', 'object2', ...]
        unary_triples: [{ attribute: 'adj', subject: 0 }, ...]
        binary_triples: [{ predicate: 'in', subject: 0, object: 2 }, ...]
    },
    ...
]
"""

import json
import sys

f = open('../data/consolidated_graph_annotations.json')
data = json.load(f)
f.close()

annotation_graphs = []
for d in data:
    annotations = d["annotations"]

    entities = []
    for entity in annotations["objects"]:
        for name in entity["names"]:
            name = str(name)
            if name not in entities:
                entities.append(name)

    unary_triples = []
    for unary_triple in annotations["unary_triples"]:
        unary_triples.append({
            "attribute": str(unary_triple["object"]),
            "subject": entities.index(str(unary_triple["text"][0]))
        })

    binary_triples = []
    for binary_triple in annotations["binary_triples"]:
        binary_triples.append({
            "predicate": str(binary_triple["predicate"]),
            "subject": entities.index(str(binary_triple["text"][0])),
            "object": entities.index(str(binary_triple["text"][2]))
        })

    annotation_graphs.append({
        "entities": entities,
        "unary_triples": unary_triples,
        "binary_triples": binary_triples
    })

f = open('../data/simplified_graph_annotations.json', 'w')
f.write(json.dumps(annotation_graphs))
f.close()
