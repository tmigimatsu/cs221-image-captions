import json

def load_everything():
  f = open('data/sentence_graphs.json', 'r')
  sentences_graphs = json.load(f)
  f.close()

  f = open('data/sentence_to_image_mapping.json', 'r')
  sentence_to_image_mapping = json.load(f)
  f.close()

  f = open('data/image_to_sentence_mapping.json', 'r')
  image_to_sentence_mapping = json.load(f)
  f.close()

  f = open('data/consolidated_graph_annotations.json', 'r')
  graph_annotations = json.load(f)
  f.close()

  return sentences_graphs, sentence_to_image_mapping, image_to_sentence_mapping, graph_annotations 
