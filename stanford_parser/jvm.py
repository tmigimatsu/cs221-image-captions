import os, jpype


def start():
    #os.environ.setdefault("STANFORD_PARSER_HOME", "/Users/ranjaykrishna/Documents/workspace/sentence_image/parser/stanford-parser-2010-08-20")
    #os.environ.setdefault("STANFORD_PARSER_HOME", "/Users/ranjaykrishna/Documents/workspace/sentence_image/parser/stanford-parser-full-2014-10-31")
    os.environ.setdefault("STANFORD_PARSER_HOME", "stanford_parser/stanford-parser-full-2014-10-31")
    stanford_parser_home = os.environ["STANFORD_PARSER_HOME"]

    jars = ["stanford-parser.jar", "stanford-parser-3.5.0-models.jar"]
    path = ";".join([stanford_parser_home + "/" + jar for jar in jars])
    #path = stanford_parser_home + "/stanford-parser.jar"
    #path = stanford_parser_home + "stanford-parser-3.5.0-models.jar"

    jpype.startJVM(jpype.getDefaultJVMPath(), "-ea", "-Djava.class.path=%s" % (path))
    #jpype.startJVM(jpype.getDefaultJVMPath(), "-ea", "-Djava.class.path=parser/stanford-parser-full-2014-10-31/stanford-parser.jar")

def close():
    print ""
    jpype.shutdownJVM()
