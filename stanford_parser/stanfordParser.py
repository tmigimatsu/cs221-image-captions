import jpype

class Parser:
    def __init__(self):
        self.lexparser = jpype.JPackage("edu.stanford.nlp.parser.lexparser")

        self.lp = self.lexparser.LexicalizedParser.loadModel()
        self.tlp = self.lp.getOp().langpack()
        self.gsf = self.tlp.grammaticalStructureFactory()
        print ""

    def parse(self, sentence, verbose=False):
        """
        Parses the sentence string, returning the tokens, and the parse tree as a tuple.
        tokens, tree = parser.parse(sentence)
        """
        if sentence[-1] == ".":
            sentence = sentence[:-1].lower()
        StringReader = jpype.JClass("java.io.StringReader")
        sr = StringReader(sentence)

        tokenizer = self.tlp.getTokenizerFactory().getTokenizer(sr)
        tokens = tokenizer.tokenize()

        tree = self.lp.parse(tokens)
        if verbose:
            tree.pennPrint()
            print ""

        return self.extract(sentence, tree, verbose)

    def extract(self, sentence, tree, verbose=False):
        # Find entities
        taggedWords = tree.taggedYield()
        entities = [str(taggedWord.word()) for taggedWord in taggedWords if
                taggedWord.tag()[:2] == "NN"]

        # Find unary, binary triples
        gs = self.gsf.newGrammaticalStructure(tree)
        typedDependencies = gs.typedDependenciesCCprocessed()
        unary_triples = []
        binary_triples = []
        noun_compounds = []

        def findObjects(predicate, relations):
            objects = []
            for td in typedDependencies:
                if str(td.gov().word()) != predicate:
                    continue
                reln = str(td.reln().toString())
                if reln[:5] in relations:
                    reln2 = reln[5:].replace("_", " ")
                    dep = str(td.dep().word())
                    objects.append((reln2, dep))
            return objects

        for i, td in enumerate(typedDependencies):
            dep = str(td.dep().word())
            gov = str(td.gov().word())
            if dep not in entities and gov not in entities:
                continue
            reln = str(td.reln().toString())
            try:
                if reln == "nn": # noun compound modifier
                    noun_compounds.append((entities.index(gov), entities.index(dep), dep + " " + gov))
                elif reln == "amod": # adjectival modifier
                    unary_triples.append({
                        "subject": entities.index(gov),
                        "attribute": dep
                    })
                elif reln == "vmod": # verbal modifier
                    compoundPhrase = False
                    for reln2, obj in findObjects(dep, ["prep_"]): # women sitting on chairs
                        compoundPhrase = True
                        if len(reln2) == 0:
                            reln2 = dep
                        binary_triples.append({
                            "subject": entities.index(gov),
                            "object": entities.index(obj),
                            "predicate": reln2
                        })
                    if not compoundPhrase: # women talking
                        unary_triples.append({
                            "subject": entities.index(gov),
                            "attribute": dep
                        })

                elif reln[:5]  == "prep_":
                    binary_triples.append({
                        "subject": entities.index(gov),
                        "object": entities.index(dep),
                        "predicate": reln[5:].replace("_", " ")
                    })
                elif reln == "agent": # agent("thrown by him") = (thrown, him)
                    for obj in findObjects(gov, ["nsubpass"]):
                        binary_triples.append({
                            "subject": entities.index(dep),
                            "object": entities.index(obj),
                            "predicate": gov
                        })
                elif reln == "nsubj": # nominal subject("baby is cute") = (cute, baby)
                    compoundPhrase = False
                    for reln2, obj in findObjects(gov, ["dobj", "iobj", "prep_"]):
                        compoundPhrase = True
                        if len(reln2) == 0:
                            reln2 = gov
                        binary_triples.append({
                            "subject": entities.index(dep),
                            "object": entities.index(obj),
                            "predicate": reln2
                        })
                    if not compoundPhrase:
                        unary_triples.append({
                            "subject": entities.index(dep),
                            "attribute": gov
                        })
                elif reln == "dep":
                    if dep in entities: # noun compound modifier (nn)
                        unary_triples.append({
                            "subject": entities.index(gov),
                            "attribute": dep
                        })
                    else:
                        for reln2, obj in findObjects(dep, ["dobj", "iobj", "prep_"]):
                            if len(reln2) > 0:
                                reln2 = " " + reln2
                            binary_triples.append({
                                "subject": entities.index(gov),
                                "object": entities.index(obj),
                                "predicate": dep + reln2
                            })
                #TODO neg, nn, conj_
            except ValueError:
                continue

            if verbose:
                print td.reln(), td.gov(), td.dep()

        for i, j, nn in noun_compounds:
            entities[i] = nn
            entities[j] = nn

        if verbose:
            print ""
            print "objects: ", entities
            print "unary_triples: ", unary_triples
            print "binary_triples: ", binary_triples
            print ""

        return {
            "objects": entities,
            "unary_triples": unary_triples,
            "binary_triples": binary_triples
        }
