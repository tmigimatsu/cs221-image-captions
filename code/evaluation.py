from __future__ import division
import numpy

"""
The functions in this file will be responsible for all the evaluation of the different ranking methods.
"""

class Evaluate:
  """
  @param image_to_sentence_mapping is a map from image_id to the list of sentence_ids that describe
         the image.
  @param sentence_to_image_mapping is a map from sentences to the image_id it describes.
  """
  def __init__(self, image_to_sentence_mapping, sentence_to_image_mapping):
    self.image_to_sentence_mapping = image_to_sentence_mapping
    self.sentence_to_image_mapping = sentence_to_image_mapping
    self.sentence_retrieval_stats = {
        'Recall @ 1': 0,
        'Recall @ 5': 0,
        'Recall @ 10': 0,
        'Median': 0,
        'Mean': 0
    }
    self.image_retrieval_stats = {
        'Recall @ 1': 0,
        'Recall @ 5': 0,
        'Recall @ 10': 0,
        'Median': 0,
        'Mean': 0
    }

  def get_image_retrieval_stats(self):
    print self.image_retrieval_stats

  def get_sentence_retrieval_stats(self):
    print self.sentence_retrieval_stats

  """
  This function will evaluate the sentences ranked in sentence_ids on the image ids in image_ids.
  @param image_ids is a list of image ids for which the retrieval was done.
  @param sentence_ids is a list of list of sentence ids where sentence_ids[i] is the list of
         sentences ranked for image_ids[i].
  """
  def evaluate_sentence_retrieval(self, image_ids, sentence_ids):
    ranks = []
    for index in range(len(image_ids)):
      image_id = image_ids[index]
      sentences = sentence_ids[index]
      correct_sentences = self.image_to_sentence_mapping[image_id]
      recall_1_count = 0
      recall_5_count = 0
      recall_10_count = 0
      number = 1
      for sentence in sentences:
        if sentence in correct_sentences:
          if number <= 1:
            recall_1_count += 1
          if number <= 5:
            recall_5_count += 1
          if number <= 10:
            recall_10_count += 1
          ranks.append(number)
        number += 1
      self.sentence_retrieval_stats['Recall @ 1'] += float(recall_1_count)
      self.sentence_retrieval_stats['Recall @ 5'] += float(recall_5_count)
      self.sentence_retrieval_stats['Recall @ 10'] += float(recall_10_count)
    self.sentence_retrieval_stats['Recall @ 1'] /= index
    self.sentence_retrieval_stats['Recall @ 5'] /= 5*index
    self.sentence_retrieval_stats['Recall @ 10'] /= 10*index
    self.sentence_retrieval_stats['Median'] = numpy.median(numpy.array(ranks))
    self.sentence_retrieval_stats['Mean'] = float(sum(ranks))/len(ranks)

  """
  This function evauates the images ranked in image_ids on the sentence ids in sentence_ids.
  @param sentence_ids is a list of sentence_ids that were used for retrieval.
  @param image_ids is a list of lists of image ids where image_ids[i] returns the image_ids
         ranked for sentence_ids[i].
  """
  def evaluate_image_retrieval(self, sentence_ids, image_ids):
    ranks = []
    for index in range(len(sentence_ids)):
      images = image_ids[index]
      sentence_id = sentence_ids[index]
      correct_image = self.sentence_to_image_mapping[sentence_id]
      number = images.index(correct_image)
      if number <= 1:
        self.sentence_retrieval_stats['Recall @ 1'] += 1.0
      if number <= 5:
        self.sentence_retrieval_stats['Recall @ 5'] += 1.0
      if number <= 10:
        self.sentence_retrieval_stats['Recall @ 10'] += 1.0
      ranks.append(number)
    self.image_retrieval_stats['Recall @ 1'] /= index
    self.image_retrieval_stats['Recall @ 5'] /= 5*index
    self.image_retrieval_stats['Recall @ 10'] /= 10*index
    self.image_retrieval_stats['Median'] = numpy.median(numpy.array(ranks))
    self.image_retrieval_stats['Mean'] = float(sum(ranks))/len(ranks)


