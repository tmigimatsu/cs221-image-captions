"""
This file contains the Random model. Given an image, it chooses a random sentence and vise versa.
"""
import copy
import random

class RandomModel:
  def sentence_retrieval(self, image_ids, sentence_ids):
    sentence_ranks = []
    for _ in image_ids:
      c = copy.copy(sentence_ids)
      random.shuffle(c)
      sentence_ranks.append(c)
    return sentence_ranks

  def image_retrieval(self, sentence_ids, image_ids):
    image_ranks = []
    for _ in sentence_ids:
      c = copy.copy(image_ids)
      random.shuffle(c)
      image_ranks.append(c)
    return image_ranks
