from __future__ import division
import json, collections, util, copy, random

class basicCSPModel:
	def __init__(self, similarityModel):
		self.solver = BacktrackingSearch()
		self.similarityModel = similarityModel
		
	def similarityMeasure(self, x, y):
		try:
			return self.similarityModel.similarity(x, y) 
		except KeyError:
			# word not in the training model, give it a random similarity
			return  random.uniform(-1, 1)
	
	def match(self, inputVariables0, inputDomain0):
		"""
		@param variable/domain is a list of strings, each represent an word		
		@return (optimal weight returned by csp solver) / (length_diff_factor)
		"""
		if len(inputDomain0) < len(inputVariables0):
			inputVariables = inputDomain0
			inputDomain = inputVariables0
		else:
			inputDomain = inputDomain0
			inputVariables = inputVariables0

		input_var_len = len(inputVariables)
		input_domain_len = len(inputDomain)
		length_diff = abs(input_var_len - input_domain_len)
		length_diff_factor = (1 - length_diff / input_var_len) if length_diff > 0 else 1.0

		domain = list()
		for i in range(input_domain_len):
			domain.append((i, inputDomain[i]))
			
		# make sure each variable can choose to not get assigned
#		if input_domain_len < input_var_len:
#			for i in range(input_domain_len, input_var_len):
#				domain.append((i, ''))
		
		csp = util.CSP()
		
		variables = list()
		for index in range(input_var_len):
			# it is possible that a word repeat in one sentence/image.
			varName = (index, inputVariables[index])
			variables.append(varName)
			csp.add_variable(varName, domain)

		for var1 in variables:
			csp.add_unary_potential(var1, lambda x : self.similarityMeasure(var1[1], x[1]))
			for var2 in variables:
				if var1 != var2:
					csp.add_binary_potential(var1, var2, lambda x, y : x != y)

		self.solver.solve(csp, mcv = True, mac = True)
		return self.solver.optimalWeight * length_diff_factor

# A backtracking algorithm that solves weighted CSP.
# Usage:
#   search = BacktrackingSearch()
#   search.solve(csp)
class BacktrackingSearch():

    def reset_results(self):
        """
        This function resets the statistics of the different aspects of the
        CSP solver. We will be using the values here for grading, so please
        do not make any modification to these variables.
        """
        # Keep track of the best assignment and weight found.
        self.optimalAssignment = {}
        self.optimalWeight = 0

        # Keep track of the number of optimal assignments and assignments. These
        # two values should be identical when the CSP is unweighted or only has binary
        # weights.
        self.numOptimalAssignments = 0
        self.numAssignments = 0

        # Keep track of the number of times backtrack() gets called.
        self.numOperations = 0

        # Keep track of the number of operations to get to the very first successful
        # assignment (doesn't have to be optimal).
        self.firstAssignmentNumOperations = 0

        # List of all solutions found.
        self.allAssignments = []

    def print_stats(self):
        """
        Prints a message summarizing the outcome of the solver.
        """
        if self.optimalAssignment:
            print "Found %d optimal assignments with weight %f in %d operations" % \
                (self.numOptimalAssignments, self.optimalWeight, self.numOperations)
            print "First assignment took %d operations" % self.firstAssignmentNumOperations
        else:
            print "No solution was found."

    def get_delta_weight(self, assignment, var, val):
        """
        Given a CSP, a partial assignment, and a proposed new value for a variable,
        return the change of weights after assigning the variable with the proposed
        value.

        @param assignment: A list of current assignment. len(assignment) should
            equal to self.csp.numVars. Unassigned variables have None values, while an
            assigned variable has the index of the value with respect to its
            domain. e.g. if the domain of the first variable is [5,6], and 6
            was assigned to it, then assignment[0] == 1.
        @param var: Index of an unassigned variable.
        @param val: Index of the proposed value with respect to |var|'s domain.

        @return w: Change in weights as a result of the proposed assignment. This
            will be used as a multiplier on the current weight.
        """
        assert assignment[var] is None
        w = 1.0
        if self.csp.unaryPotentials[var]:
            w *= self.csp.unaryPotentials[var][val]
            if w == 0: return w
        for var2, potential in self.csp.binaryPotentials[var].iteritems():
            if assignment[var2] == None: continue  # Not assigned yet
            w *= potential[val][assignment[var2]]
            if w == 0: return w
        return w

    def solve(self, csp, mcv = False, lcv = False, mac = False):
        """
        Solves the given weighted CSP using heuristics as specified in the
        parameter. Note that unlike a typical unweighted CSP where the search
        terminates when one solution is found, we want this function to find
        all possible assignments. The results are stored in the variables
        described in reset_result().

        @param csp: A weighted CSP.
        @param mcv: When enabled, Most Constrained Variable heuristics is used.
        @param lcv: When enabled, Least Constraining Value heuristics is used.
        @param mac: When enabled, AC-3 will be used after each assignment of an
            variable is made.
        """
        # CSP to be solved.
        self.csp = csp

        # Set the search heuristics requested asked.
        self.mcv = mcv
        self.lcv = lcv
        self.mac = mac

        # Reset solutions from previous search.
        self.reset_results()

        # The list of domains of every variable in the CSP. Note that we only
        # use the indices of the values. That is, if the domain of a variable
        # A is [2,3,5], then here, it will be stored as [0,1,2]. Original domain
        # name/value can be obtained from self.csp.valNames[A]
        self.domains = [list(range(len(domain))) for domain in self.csp.valNames]

        # Perform backtracking search.
        self.backtrack([None] * self.csp.numVars, 0, 1)

        # Print summary of solutions.
#        self.print_stats()

    def backtrack(self, assignment, numAssigned, weight):
        """
        Perform the back-tracking algorithms to find all possible solutions to
        the CSP.

        @param assignment: A list of current assignment. len(assignment) should
            equal to self.csp.numVars. Unassigned variables have None values, while an
            assigned variable has the index of the value with respect to its
            domain. e.g. if the domain of the first variable is [5,6], and 6
            was assigned to it, then assignment[0] == 1.
        @param numAssigned: Number of currently assigned variables
        @param weight: The weight of the current partial assignment.
        """

        self.numOperations += 1
        assert weight > 0
        if numAssigned == self.csp.numVars:
            # A satisfiable solution have been found. Update the statistics.
            self.numAssignments += 1
            newAssignment = {}
            for var in range(self.csp.numVars):
                newAssignment[self.csp.varNames[var]] = self.csp.valNames[var][assignment[var]]
            self.allAssignments.append(newAssignment)

            if len(self.optimalAssignment) == 0 or weight >= self.optimalWeight:
                if weight == self.optimalWeight:
                    self.numOptimalAssignments += 1
                else:
                    self.numOptimalAssignments = 1
                self.optimalWeight = weight

                self.optimalAssignment = newAssignment
                if self.firstAssignmentNumOperations == 0:
                    self.firstAssignmentNumOperations = self.numOperations
            return

        # Select the index of the next variable to be assigned.
        var = self.get_unassigned_variable(assignment)

        # Least constrained value (LCV) is not used in this assignment
        # so just use the original ordering
        ordered_values = self.domains[var]

        # Continue the backtracking recursion using |var| and |ordered_values|.
        if not self.mac:
            # When arc consistency check is not enabled.
            for val in ordered_values:
                deltaWeight = self.get_delta_weight(assignment, var, val)
                if deltaWeight > 0:
                    assignment[var] = val
                    self.backtrack(assignment, numAssigned + 1, weight * deltaWeight)
                    assignment[var] = None
        else:
            # Arc consistency check is enabled.
            # Problem 1c: skeleton code for AC-3
            # You need to implement arc_consistency_check().
            for val in ordered_values:
                deltaWeight = self.get_delta_weight(assignment, var, val)
                if deltaWeight > 0:
                    assignment[var] = val
                    # create a deep copy of domains as we are going to look
                    # ahead and change domain values
                    localCopy = copy.deepcopy(self.domains)
                    # fix value for the selected variable so that hopefully we
                    # can eliminate values for other variables
                    self.domains[var] = [val]

                    # enforce arc consistency
                    self.arc_consistency_check(var)

                    self.backtrack(assignment, numAssigned + 1, weight * deltaWeight)
                    # restore the previous domains
                    self.domains = localCopy
                    assignment[var] = None

    def get_unassigned_variable(self, assignment):
        """
        Given a partial assignment, return the index of a currently unassigned
        variable.

        @param assignment: A list of current assignment. This is the same as
            what you've seen so far.

        @return var: Index of a currently unassigned variable.
        """

        if not self.mcv:
            # Select a variable without any heuristics.
            for var in xrange(len(assignment)):
                if assignment[var] is None: return var
        else:
            # Problem 1b
            # Heuristic: most constrained variable (MCV)
            # Select a variable with the least number of remaining domain values.
            # Hint: remember to use indices: for var_idx in range(len(assignment)): ...
            # Hint: given var_idx, self.domains[var_idx] gives you all the possible values
            # BEGIN_YOUR_CODE (around 10 lines of code expected)
            leastNumConsistentChoice = float('inf')
            best_var_ind = -1
            for var_ind in xrange(len(assignment)):
            	if assignment[var_ind] is None:
            		options = 0
            		for val in self.domains[var_ind]:
            			if self.get_delta_weight(assignment, var_ind, val) > 0:
            				options += 1
            		if options < leastNumConsistentChoice:
            			leastNumConsistentChoice = options
            			best_var_ind = var_ind
            return best_var_ind
            # END_YOUR_CODE


    def arc_consistency_check(self, var):
        """
        Perform the AC-3 algorithm. The goal is to reduce the size of the
        domain values for the unassigned variables based on arc consistency.

        @param var: The index of variable whose value has just been set.
        """
        # Problem 1c
        # Hint: How to get indices of variables neighboring variable at index |var|?
        # => for var2 in self.csp.get_neighbor_vars(var):
        #       # use var2
        #
        # Hint: How to check if two values are inconsistent?
        # For unary potentials (var1 is the index, val1 is the value index):
        #   => self.csp.unaryPotentials[var1][val1] == 0
        #
        # For binary potentials (var1 and var2 are indices, val1 and val2 are value indices)::
        #   => self.csp.binaryPotentials[var1][var2][val1][val2] == 0
        #   (self.csp.binaryPotentials[var1][var2] returns a nested dict of all assignments)

        # BEGIN_YOUR_CODE (around 20 lines of code expected)
        q = list()
        q.append(var)
        while len(q) > 0:
        	var1 = q.pop()
        	var1Domain = self.domains[var1]
        	for var2 in self.csp.get_neighbor_vars(var1):
        		curNeighborDomain = copy.deepcopy(self.domains[var2])
        		newNeighborDomain = []
        		for var2val in curNeighborDomain:
        			if self.csp.unaryPotentials[var2] and self.csp.unaryPotentials[var2][var2val] == 0:
        				continue
        			for var1val in var1Domain:
        				if self.csp.binaryPotentials[var1][var2][var1val][var2val] != 0:
        					newNeighborDomain.append(var2val)
        					break   # consistent with at least one val for var1
        		newNeighborDomain.sort()
        		curNeighborDomain.sort()
        		if curNeighborDomain != newNeighborDomain:
        			self.domains[var2] = newNeighborDomain
        			q.append(var2)
        # END_YOUR_CODE

