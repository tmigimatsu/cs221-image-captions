from greedy_model import GreedyModel
from evaluation import Evaluate

import gensim
import json

"""
Collect all the data.
"""
f = open('data/sentence_graphs.json', 'r')
sentences = json.load(f)
f.close()

f = open('data/consolidated_graph_annotations.json', 'r')
graphs = json.load(f)
f.close()

f = open('data/sentence_to_image_mapping.json', 'r')
sentence_to_image_mapping = json.load(f)
f.close()

f = open('data/image_to_sentence_mapping.json', 'r')
image_to_sentence_mapping = json.load(f)
f.close()

"""
Import everything to the model
"""
image_ids = [0]
sentence_ids = [0]

print "Creating Model..."
word_model = gensim.models.Word2Vec.load_word2vec_format('data/GoogleNews.bin', binary=True)
greedyModel = GreedyModel(sentences, graphs, word_model)
print "Runing Sentence Retrieval..."
sentence_rankings = greedyModel.sentence_retrieval(image_ids)
print "Runing Image Retrieval..."
image_rankings = greedyModel.image_retrieval(sentence_ids)

"""
Run evaluation
"""
print "Running Evaluation..."
evaluate = Evaluate(image_to_sentence_mapping, sentence_to_image_mapping)
evaluate.evaluate_sentence_retrieval(image_ids, sentence_rankings)
print evaluate.get_sentence_retrieval_stats()
evaluate.evaluate_image_retrieval(sentence_ids, image_rankings)
print evaluate.get_image_retrieval_stats()
