from collections import Counter
import copy
import gensim

class GreedyModel:
  def __init__(self, sentence_graphs, image_graphs, word_model):
    self.sentence_graphs = sentence_graphs
    self.image_graphs = image_graphs
    self.model = word_model

  def sentence_retrieval(self, image_ids):
    return self.graph_retrieval(image_ids, self.image_graphs, self.sentence_graphs)

  def image_retrieval(self, sentence_ids):
    return self.graph_retrieval(sentence_ids, self.sentence_graphs, self.image_graphs)

  def graph_retrieval(self, query_ids, query_graphs, test_graphs):
    rankings = []
    for query_id in query_ids:
      query_graph = query_graphs[query_id]
      rank_map = Counter()
      index = 0
      for test_graph in test_graphs:
        value = self.measure_similarity(query_graph, test_graph)
        rank_map[index] = value
        index += 1
      rankings.append([elem[0] for elem in rank_map.most_common()])
    return rankings

  def measure_similarity(self, query, test):
    if 'annotations' in query:
      query = query['annotations']
    if 'annotations' in test:
      test = test['annotations']
    measure = 0
    measure += self.measure_similarity_lists(query['objects'], test['objects'])
    #measure += self.measure_similarity_lists(query['unary_triples'], test['unary_triples'])
    #measure += self.measure_similarity_lists(query['binary_triples'], test['binary_triples'])
    return measure

  def measure_similarity_lists(self, query, test):
    print test
    test = copy(test)
    measure = 0
    for obj in query:
      if len(test) <= 0:
        break
      curr_value = 0
      curr_t = None
      for t in test:
        value = self.measure_similarity_string(obj, t)
        if value > curr_value:
          curr_value = value
          curr_t = t
      measure += curr_value
      test.remove(curr_t)
    return measure

  def measure_similarity_string(self, query, test):
    query = query.split(' ')[0]
    test = test.split(' ')[0]
    if query in self.model and test in self.model:
      return self.model.similarity(query, test)
    return 0


