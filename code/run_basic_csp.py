from cspModel import basicCSPModel
from gensim.models import Word2Vec
from evaluation import Evaluate
from multiprocessing import Pool

import random
import sys
import json

"""
Run a multi process program for CSP: python code/run_basic_csp.py num_proccesses
"""

def convertImageGraphletToWords(graphlets):
	imageWordsList = list()
	for image_graphlet in graphlets:
		wordsForImage = list()
		for sens in [c['names'] for c in image_graphlet['annotations']['objects']]:
			for sen in sens:
				wordsForImage.append(sen);
		imageWordsList.append(wordsForImage)
	return imageWordsList
	
def convertSentenceGraphletToWords(graphlets):
	sentenceObjectsList = list()
	for graphlet in graphlets:
		sentenceObjectsList.append(graphlet['objects'])
	return sentenceObjectsList
	

"""
Helper functions for multi processsing
"""

def matchWithIndex((model, id1, id2, graph1, graph2)):
	print "=========="
	print len(graph1)
	print len(graph2)
	weight = model.match(graph1, graph2)
	print weight
	return id1, id2, weight

def createJobs(model, graphlet1, graphlet2):
	jobs = list()
	for id1, g1 in graphlet1:
		for id2, g2 in graphlet2:
			jobs.append((model, id1, id2, g1, g2))
	print "Job length:"
	print len(jobs)
	return jobs
					
def convertJobResultsToRanking(results):
	d = dict()
	for (id1, id2, weight) in results:
		if d.has_key(id1):
			d[id1][id2] = weight
		else:
			d[id1] = {id2: weight}
	convertedResults = list()
	for outterId, innerDict in sorted(d.items()):
		innerList = list()
		for innerId, weight in sorted(innerDict.items(), key=lambda x: -x[1]): # order by descending weights
			innerList.append(innerId)
		convertedResults.append(innerList)
	return convertedResults
	
def sentence_retrieval(poolSize, model, imageWords, sentenceWords):
	pool = Pool(processes=poolSize)
	results = pool.map(matchWithIndex, createJobs(model, imageWords, sentenceWords))
	pool.close()
	pool.join()
	return convertJobResultsToRanking(results)
		
def image_retrieval(poolSize, model, imageWords, sentenceWords):
	pool = Pool(processes=poolSize)
	results = pool.map(matchWithIndex, createJobs(model, sentenceWords, imageWords))
	pool.close()
	pool.join()
	return convertJobResultsToRanking(results)
	
	

WORD2VEC_MODEL_FILE_NAME = '/Users/annani/Downloads/GoogleNews-vectors-negative300.bin'
GRAPH_TRAINED_MODEL = 'data/GraphObjectsTrainedWord2vecModel.txt'

"""
Collect all the data.
"""

f = open('data/sentence_graphs.json', 'r')
raw_sentence_graphlets = json.load(f)
f.close()

f = open('data/consolidated_graph_annotations.json', 'r')
raw_image_graphlets = json.load(f)
f.close()

f = open('data/sentence_to_image_mapping.json', 'r')
sentence_to_image_mapping = json.load(f)
f.close()

f = open('data/image_to_sentence_mapping.json', 'r')
image_to_sentence_mapping = json.load(f)
f.close()

"""
Get word2vecModel
"""
#word2vecModel = Word2Vec.load_word2vec_format(WORD2VEC_MODEL_FILE_NAME, binary=True)
word2vecModel = Word2Vec.load(GRAPH_TRAINED_MODEL)

model = basicCSPModel(word2vecModel)


poolSize = int(sys.argv[1])
imageSize = int(sys.argv[2])

raw_image_words = convertImageGraphletToWords(raw_image_graphlets)
raw_sentence_words = convertSentenceGraphletToWords(raw_sentence_graphlets)

image_ids = sorted(random.sample(range(len(raw_image_graphlets)), imageSize))

sentences = set()
for image_id in image_ids:
	for sentence_id in image_to_sentence_mapping[image_id]:
		sentences.add(sentence_id)
sentence_ids = sorted(list(sentences))

print "start..."
print "number of sentences: %d" % len(sentence_ids)
print "number of images: %d" % len(image_ids)

imageWords = [(image_id, raw_image_words[image_id]) for image_id in image_ids]
sentenceWords = [(sentence_id, raw_sentence_words[sentence_id]) for sentence_id in sentence_ids]

evaluate = Evaluate(image_to_sentence_mapping, sentence_to_image_mapping)

sentence_rankings = sentence_retrieval(poolSize, model, imageWords, sentenceWords)
evaluate.evaluate_sentence_retrieval(image_ids, sentence_rankings)
evaluate.get_sentence_retrieval_stats()

image_rankings = image_retrieval(poolSize, model, imageWords, sentenceWords)
evaluate.evaluate_image_retrieval(sentence_ids, image_rankings)
evaluate.get_image_retrieval_stats()




