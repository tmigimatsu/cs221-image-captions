from random_model import RandomModel
from evaluation import Evaluate

import json

"""
Collect all the data.
"""
f = open('data/sentences.json', 'r')
sentences = json.load(f)
f.close()

f = open('data/consolidated_graph_annotations.json', 'r')
graphs = json.load(f)
f.close()

f = open('data/sentence_to_image_mapping.json', 'r')
sentence_to_image_mapping = json.load(f)
f.close()

f = open('data/image_to_sentence_mapping.json', 'r')
image_to_sentence_mapping = json.load(f)
f.close()

"""
Create the image and sentence ids
"""
image_ids = range(len(graphs))
sentence_ids = range(len(sentences))

randomModel = RandomModel()
sentence_rankings = randomModel.sentence_retrieval(image_ids, sentence_ids)
image_rankings = randomModel.image_retrieval(sentence_ids, image_ids)

evaluate = Evaluate(image_to_sentence_mapping, sentence_to_image_mapping)
evaluate.evaluate_sentence_retrieval(image_ids, sentence_rankings)
print evaluate.get_sentence_retrieval_stats()
evaluate.evaluate_image_retrieval(sentence_ids, image_rankings)
print evaluate.get_image_retrieval_stats()
