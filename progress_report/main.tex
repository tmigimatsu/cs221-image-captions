\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}

\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref} 

\cvprfinalcopy

\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Graphlets: Capturing Semantic Relationships Between Objects in Images}

\author{
  Ranjay Krishna\\
  Stanford University\\
  {\tt\small rak248@stanford.edu}
\and
  Bojiong Ni\\
  Stanford University\\
  {\tt\small bojiong@stanford.edu}
\and
  Takatoki Migimatsu\\
  Stanford University\\
  {\tt\small takatoki@stanford.edu}
}

\maketitle

%%%%%%%%% ABSTRACT
\begin{abstract}
A necessary goal of artificial intelligence is to comprehend the semantic
information contained within an image and relate it to sentences. Understanding
the composition of images improves scene understanding, which in turn improves
applications like Scene Retrieval, Image Captioning and Scene Categorization.
We use a richly annotated corpus of natural scenes where we reason about each
image through its corresponding scene \textbf{graphlet}. The scene graphlet for
an image includes human-annotated objects along with attributes and pairwise
binary relationships between objects. In particular, we show that using a model
that incorporates these relationships improves the performance of sentence and
scene retrieval. We compare baseline methods for retrieval with methods
incorporating our graphlets and show that we can retrieve images that
correspond strongly to the semantic meaning expressed by the scene graphlets.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\label{introduction}
Computer vision has made advancements in a multitude of different tasks ranging
from classification \cite{deng2009imagenet} to image-sentence alignment
\cite{hodosh2013framing} and \cite{socher2010connecting}. As the field of
computer vision advances, one of the fundamental problems that needs to be
tackled is the semantic understanding of images. Distinguishing the objects in
an image is only the first step towards semantic understanding. The next step is
to learn the relationships that arise between these objects. These relationships
can be spatial, where an object \textit{man} is \textit{next to} another object
\textit{man}, or possessive, where an \textit{arm} is \textit{part of}
\textit{man}. With these relationships, we can build models that can learn to
predict further relationships and improve applications like scene retrieval.

Recently, there has been a significant amount of work in image annotation and
understanding. \cite{farhadi2009describing}, \cite{lampert2009learning} and
\cite{parikh2011relative} explore attribute based representations.
\cite{gupta2008beyond} uses both prepositions (binary relationships) and
adjectives (attributes) to build better object models. Meanwhile,
\cite{sadeghi2011recognition} and \cite{yao2011classifying} explore binary
relationships to convey actions and affordances. Our work is most similar to
\cite{zitnick2013learning}, which uses abstract scenes to study the
relationship between words and visual features. Unlike their work, we focus
primarily on the binary relationships and use a dataset that consists of
natural images.

\begin{figure}[t]
\centering
%\fbox{\rule{0pt}{2in} \rule{0.9\linewidth}{0pt}}
   \includegraphics[width=0.8 \linewidth]{pull_figure.png}
   \caption{An example of a scene graphlet along with two images whose objects
   correspond with the nodes and edges in the graphlet.}
\label{fig:pull_figure}
\end{figure}

In this paper, we study the value of grounded binary relationships between
objects in an image. For example, in Figure ~\ref{fig:pull_figure}, we show the
groundings of the same scene graphlet in two separate images. The Computer
Vision community still does not have perfect, fine-grained object detectors. By
using grounded binary relationships in images, we avoid the difficulties of
having to detect every single object, and instead, concentrate on exploiting
these relationships.

The images we use are a subset of the corpus introduced in
\cite{lin2014microsoft}. The object annotations of these images were gathered by
\cite{visualgenome}. These objects were annotated as part of either a unary or
binary relationship. Unary relationships refer to the attributes associated with
each object (e.g. adjectival modifiers), while binary relationships capture the
information expressed by the occurrence of a pair of objects (e.g.
prepositional phrases). The result is a dataset of 5000 images with unary and
binary relationships of objects that are grounded in the image with bounding
boxes.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{dataset_comparison.png}
  \caption{A distribution of the number of objects (a), attributes (b) and
  relationships (c) to the number of images in our dataset.}
  \label{fig:dataset_comparison}
\end{figure*}

Our approach models an image using a Conditional Random Field (CRF) with each
node corresponding to an object. The attributes of each object model the unary
potentials for its node. Finally, the pairwise potentials model the edges
between nodes corresponding to the binary relationships that occur between
objects in a scene. For each scene graphlet, we extract attribute features like
material and color for each object to determine its unary potential. Likewise,
we extract spatial features from binary relationships to calculate the pairwise
potential between nodes. Results show that our approach can retrieve images
that correspond to the semantic meaning expressed by the scene graphlet. We
show that this model significantly improves the task of scene and sentence
retrieval over our baseline.

\section{Related Work}
\label{related_work}
\paragraph{Datasets.} Datasets have long played a role in the development of new
computer vision algorithms. \cite{xiao2010sun} helped push forward the research
on scene classification while \cite{deng2009imagenet, russell2008labelme,
everingham2010pascal} helped build better classification and detection models.
To date, the largest computer vision datasets have been object-centric.
Finally, \cite{lin2014microsoft} is a non-iconic dataset with the aim of
understanding a scene through contextual resoning and 2D localization of
objects. The next step to scene understanding also involved learning about the
attributes of objects and relationships between objects.

\paragraph{Image Retrieval.} Many approaches for image retrieval have been
explored, including clustering \cite{ben2006improvingweb,
fritz2008decomposition}, graphical models \cite{fergus2005learning,
morsillo2009semi} and SVM classification \cite{schroff2011harvesting}. Other
retrieval tasks have focused on similarity measures using GIST features
\cite{oliva2001modeling}, attributes \cite{deng2011hierarchical,
farhadi2009describing} and hamming embeddings \cite{jegou2008hamming}.

\paragraph{Structured Representation.} There has been much recent interest in
models that reason about images and natural language descriptions, ranging from
Flickr tags~\cite{li09cvpr} over sentences with canonical
structure~\cite{kulkarni2011baby,mitchell2012midge,zitnick2013learning} to
unaligned text
corpora~\cite{socher2010connecting,karpathyJF14,socher2013grounded}. These
models achieve impressive results in scene classification and object
recognition using only weak supervision. However, they are limited in terms of
expressiveness (e.g., ``baby talk''~\cite{kulkarni2011baby}). In contrast, our
scene graphlets are a structured representation of visual scenes. Each node is
explicitly grounded in an image region, avoiding the inherent referential
uncertainty of text-based representations.

\textbf{Bi-directional Mapping}. Quite a few papers have also explored the
possibilities of combining classification and detection with attributes and
relationships to communicate the contents of a scene. \cite{farhadi2010every}
extracts visual features and transforms an image into a \textit{meaning space}
in the form of triples connecting objects to retrieve sentences that correspond
to the scene. Similarly, \cite{ordonez2011im2text} extracts actions and uses a
similarity metric between images to understand them. \cite{li2011composing}
also generates sentences from scratch using n-grams of contextual information
from an image. Finally, \cite{karpathyJF14} creates a mapping between fragments
of a sentence to components of an image.

\section{Dataset}
\label{dataset}

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.98\textwidth]{fruit_image.png}
  \caption{An example of the scene graphlet extracted from a given image. We
  display the groundings of scene graphlet from the objects in the image.}
  \label{fig:fruit_image}
\end{figure*}

Our dataset consists of 5000 images manually chosen from the intersection of
the Yahoo-Flickr Creative Commons 100M dataset and the Microsoft COCO
\cite{lin2014microsoft} dataset.

We obtained object, attribute, and relationship annotations for these images
using crowd workers on Amazon Mechanical Turk. Workers are shown an image and
instructed to "write a short phrase about the image" such as "man wearing hat"
or "dog is brown". For each phrase, workers are also asked to draw bounding
boxes indicating the location in the image of the objects mentioned by the
phrase. Each image is seen by three workers, and each worker must write at
least 15 short phrases about each image. All worker-provided annotations
(objects, bounding boxes, short phrases) are independently verified by other
workers in separate tasks.

In total, our dataset consists of 93,832 annotated object instances, 115,020
instances of relationships between objects, and 112,945 object attributes.
There are 877 different categories of objects with at least 10 instances per
category, 128 types of relationships with at least 10 instances, and 514
attributes with at least 10 instances.

On average, we have around 18 image objects per image, along with 22 unary
attributes and 22 binary relationships. See Figure~\ref{fig:dataset_comparison}
for the distribution of annotations.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.98\textwidth]{sentence_parsing.png}
  \caption{Given a query sentence (a), we will first extract its typed
  dependency (b) and then convert it into a scene graphlet (c).}
  \label{fig:sentence_parsing}
\end{figure*}

\section{Model}
\label{model}
Our strategy is to convert both images and sentences into scene graphlets and
use those graphlets as queries to retrieve similar images and sentences. To do
this, we must be able to measure the agreement between a query scene graphlet
and an unannotated test image. We assume that this agreement can be measured by
examining the best possible grounding of the scene graphlet to the test image.

To this end, we construct a conditional random field (CRF~\cite{lafferty01icml})
that models the distribution over all possible groundings.  We perform maximum
a posteriori (MAP) inference to find the most likely grounding; the likelihood
of this MAP solution is taken as the score measuring the agreement between the
scene graphlet and the image.

\subsection{CRF Formulation}

Let $G=(O,E)$ be a scene graphlet, $B$ be a set of bounding boxes in an image, and
$\gamma$ a grounding of the scene graphlet to the image. Each object $o\in O$
gives rise to a variable $\gamma_o$ in the CRF, where the domain of $\gamma_o$
is $B$ and setting $\gamma_o=b\in B$ corresponds to grounding object $o$ in the
scene graphlet to box $b$ in the image. We model the distribution over possible
groundings as
{\small
\begin{equation}
  P(\gamma \mid G, B) = \prod_{o\in O}P(\gamma_o\mid o)
  \hspace{-4mm} \prod_{(o,r,o')\in E} \hspace{-4mm} P(\gamma_o, \gamma_{o'}\mid o,r,o').
\end{equation}
}

Using Bayes' rule, we may rewrite $P(\gamma_o\mid o)$ as $P(o\mid\gamma_o)
P(\gamma_o) / P(o)$. Assuming uniform priors over bounding boxes and object
classes, $P(\gamma_o)$ and $P(o)$ are constants and can be ignored when
performing MAP inference. Therefore, our final objective has the form
 \begin{equation}
   P(\alpha_o\mid o) = \frac{1}{P(o)} P(o\mid\alpha_o) P(\alpha_o)
 \end{equation}
We assume uniform priors over bounding boxes and object classes, so
$P(\alpha_o)$ and $P(o)$ are constants and can be ignored when performing MAP
inference. Therefore our objective has the form
{\small
\begin{equation}
  \label{eq:objective}
  \gamma^* = \arg\max_\gamma \prod_{o\in O}P(o\mid\gamma_o)
  \hspace{-3.5mm} \prod_{(o,r,o')\in E} \hspace{-3.5mm} P(\gamma_o,\gamma_{o'}\mid o,r,o').
\end{equation}
}

\vspace{-8mm}

\paragraph{Unary potentials.}
The term $P(o\mid \gamma_o)$ in Equation~\ref{eq:objective} is a unary
potential modeling how well the appearance of the box $\gamma_o$ agrees with
the known object class and attributes of the object $o$. If $o=(c, A)$, then we
decompose this term as
{\small
\begin{equation}
  P(o\mid\gamma_o) = P(c\mid\gamma_o)\prod_{a\in A}P(a\mid \gamma_o).
\end{equation}
}
The terms $P(c\mid \gamma_o)$ and $P(a\mid\gamma_o)$ are simply the
probabilites that the bounding box $\gamma_o$ shows object class $c$ and
attribute $a$. To model these probabilities, we use R-CNN \cite{girshick14CVPR}
to to train detectors for each of the $|\mathcal{C}|=266$ and
$|\mathcal{A}|=145$ object classes and attribute types. We apply Platt
scaling~\cite{platt99lmc} to convert the SVM classification scores for each
object class and attribute into probabilities.

\paragraph{Binary potentials.}
The term $P(\gamma_o,\gamma_{o'}\mid o,r,o')$ in Equation~\ref{eq:objective} is
a binary potential that models how well the pair of bounding boxes
$\gamma_o,\gamma_{o'}$ express the tuple $(o, r, o')$. Let $\gamma_o=(x, y, w,
h)$ and $\gamma_{o'}=(x',y',w',h')$ be the coordinates of the bounding boxes in
the image. We extract features $f(\gamma_o, \gamma_{o'})$ encoding their
relative position and scale:
{\small
\begin{equation}
  f(\gamma_o, \gamma_{o'}) = \left(\frac{x-x'}{w}, \frac{y-y'}{h}, \frac{w'}{w},
                                   \frac{h'}{h}\right)
\end{equation}
}
Suppose that the objects $o$ and $o'$ have classes $c$ and $c'$, respectively.
Using the training data, we train a Gaussian mixture model (GMM) to model
$P(f(\gamma_o,\gamma_{o'}) \mid c, r, c')$. If there are fewer than 30
instances of the tuple $(c, r, c')$ in the training data then we instead fall
back on an object agnostic model $P(f(\gamma_o,\gamma_{o'})\mid r)$. In either
case, we use Platt scaling to convert the value of the GMM density function
evaluted at $f(\gamma_o,\gamma_{o'})$ to a probability
$P(\gamma_o,\gamma_{o'}\mid o,r,o')$.

\subsection{Implementation details}
We compared the performance of several methods for generating candidate boxes
for images, including Objectness~\cite{alexe2012measuring}, Selective search
(SS~\cite{UijlingsIJCV2013}), and Geodesic Object Proposals
(GOP~\cite{krahenbuhl2014geodesic}). We use GOP for all experiments. We
perform approximate inference using off-the-shelf belief
propagation~\cite{andres2012opengm}.

\section {Evaluation}
\label{evaluation}

\subsection{Baseline}
\label{baseline}
\textbf{Random} For our first baseline model, we use a random ranking algorithm
that ignores the given sentence or image query and returns a random ranking of
all the images in the test for image retrieval and a random ranking of all the
sentences in the test set for sentence retrieval.

\textbf{Object detector}. For the sentence retrieval baseline, we use GIST
\cite{oliva2001modeling} descriptors to detect objects in the image and then
use a vectorized representation \cite{mikolov2013efficient} of the objects to
compare against all the objects contained in the sentences in our test set. We
rank the sentences based on how closely the objects correlate.

For image retrieval, we use Stanford Parser to convert the sentence into a
typed dependency list as shown in Figure~\ref{fig:sentence_parsing}. Next, we
extract the objects from the list and use a vectorized representation to match
them against the scene graphlets of our test images. We return the images in our
test set ranked according to best match based on just the objects.

\subsection{Oracle}
\label{oracle}
The oracle of our model is rather simple. We already have a mapping from the
5000 images to the 5 sentences that describe each one of them. So, given a
sentence or an image, the oracle trivially returns its corresponding image or
sentence and always achieves a perfect Recall of 1.

\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{2pt}
  \begin{tabular}{|c|c|c|c|c|}
  \hline
  & Random & GIST & Object Only & Our Model \\
  \hline
  Median rank & 460 & - & - & - \\
  Recall @ 1 & 0.000 & - & - & - \\
  Recall @ 5 & 0.000 & - & - & - \\
  Recall @ 10 & 0.002 & - & - & - \\
  \hline
  \end{tabular}
  \caption{Results after evaluating on a test set of 1000 images on a model
  trained using 4000 images.}
\end{table}

\section{Next Steps}
So far, we have set up the architecture for conducting our experiments and have
managed to extract the necessary data for our 5000 images. We have also
gathered some preliminary results as mentioned above.

Next, we intend to implement our model which incorporates the relationships
between the objects in the scene graphlets test out our CRF model. 

We intend to report quantitative results in the form of recall @ k, where k
is the number of images or sentences returned by our model. We also intend to
report qualitative measures by finding specific objects and relationships
in which our model performs the best and worst.

\section{Progress Report}

Initially, we proposed creating a model that can describe an image by
generating a caption for the image. We have pivoted from the initial proposal
that we submitted. Instead of automatically generating descriptions for an
input image, we will focus on the task of image and sentence retrieval.

Please refer to our proposal for a description of \textit{Generating
Description} and refer to Section~\ref{introduction} for image and sentence
retrieval. Please refer to Section~\ref{baseline} and Section~\ref{oracle} for
the basline and oracle models. Section~\ref{evaluation} describes our
evaluation metric, while Section~\ref{model} describes our model that we will
implement.

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
\nocite{*}
}

\end{document}
