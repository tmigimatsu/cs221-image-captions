cs221-image-captions
====================

SETUP:
Download Stanford Parser 3.5.0 and either add the
"stanford-parser-full-2014-10-31" folder to "/stanford_parser", or set the
STANFORD_PARSER_HOME environment variable to the directory of the Stanford
Parser library.

DATA:
data folder contains all the raw data
sentence.json contains all the sentences
consolidated_graphs.json contains all the image data with their bounding
boxes and attributes and relationships.
sentence_to_image_mapping.json and image_to_sentence_mapping.json are self
explanatory. These are the only files we will be dealing with. All the other
files were there for preprocessing.

CODE:
Most of the code lives in the code folder. It code to load and run different
models. But it assumes that you already have modules such as openGM for CRF 
generation, word2vec from gensim, stanford parser using one of the python 
wrapper, GIST and a few other external programs either installed in your
computer or in this directory.

SCRIPTS:
These are mainly scripts that we ran to deal with data analysis.

NOT INCLUDED:
Some of the one time scripts that we ran are scattered all around. Including
MATLAB code for GIST proposals and converting from MATLAB to json objects.

