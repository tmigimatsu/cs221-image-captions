\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}

\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref} 

\cvprfinalcopy

\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Graphlets: Capturing Semantic Relationships Between Objects in Images}

\author{
  Ranjay Krishna\\
  Stanford University\\
  {\tt\small rak248@stanford.edu}
\and
  Bojiong Ni\\
  Stanford University\\
  {\tt\small bojiong@stanford.edu}
\and
  Takatoki Migimatsu\\
  Stanford University\\
  {\tt\small takatoki@stanford.edu}
}

\maketitle

%%%%%%%%% ABSTRACT
\begin{abstract}
A necessary goal of artificial intelligence is to comprehend the semantic
information contained within an image and convey its meaning through sentences.
Understanding the composition of images improves scene understanding, which in
turn improves applications like Scene Retrieval, Image Captioning and Scene
Categorization. To that end, we use a richly annotated corpus of natural scenes
where we reason about each image through its corresponding scene
\textbf{graphlet}. The scene graphlet for an image includes human-annotated
objects along with attributes and pairwise binary relationships between
objects. In particular, we show that using a model that incorporates these
relationships improves the performance of sentence and scene retrieval. We
compare baseline methods for retrieval with methods incorporating our graphlets
and show that we can retrieve images that correspond strongly to the semantic
meaning expressed by the scene graphlets.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\label{introduction}
Computer vision has made advancements in a multitude of different tasks ranging
from classification \cite{deng2009imagenet} to image-sentence alignment
\cite{hodosh2013framing} and \cite{socher2010connecting}. As the field of
computer vision advances, one of the fundamental problems that needs to be
tackled is the semantic understanding of images. Distinguishing the objects in
an image is only the first step towards semantic understanding. The next step is
to learn the relationships that arise between these objects. These relationships
can be spatial, where an object \textit{man} is \textit{next to} another object
\textit{man}, or possessive, where an \textit{arm} is \textit{part of}
\textit{man}. With these relationships, we can build models that can learn to
predict further relationships and improve applications like scene retrieval.

Recently, there has been a significant amount of work in image annotation and
understanding. \cite{farhadi2009describing}, \cite{lampert2009learning} and
\cite{parikh2011relative} explore attribute based representations.
\cite{gupta2008beyond} uses both prepositions (binary relationships) and
adjectives (attributes) to build better object models. Meanwhile,
\cite{sadeghi2011recognition} and \cite{yao2011classifying} explore binary
relationships to convey actions and affordances. Our work is most similar to
\cite{zitnick2013learning}, which uses abstract scenes to study the
relationship between words and visual features. Unlike their work, we focus
primarily on the binary relationships and use a dataset that consists of
natural images.

\begin{figure}[t]
\centering
%\fbox{\rule{0pt}{2in} \rule{0.9\linewidth}{0pt}}
   \includegraphics[width=0.8 \linewidth]{pull_figure.png}
   \caption{An example of a scene graphlet along with two images whose objects
   correspond with the nodes and edges in the graphlet. The graphlet contains
   the objects: ``person'', ``car'', ``dog'' and ``window'' that are grounded
   in the image. The objects also have attributes like ``white'' and
   relationships between objects such as ``next to'' and ``part of''.}
\label{fig:pull_figure}
\end{figure}

In this paper, we study the value of grounded binary relationships between
objects in an image. For example, in Figure ~\ref{fig:pull_figure}, we show the
groundings of the same scene graphlet in two separate images. The Computer
Vision community still does not have perfect, fine-grained object detectors. By
using grounded binary relationships in images, we avoid the difficulties of
having to detect every single object, and instead, concentrate on exploiting
these relationships.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{dataset_comparison.png}
  \caption{A distribution of the number of objects (a), attributes (b) and
  relationships (c) to the number of images in our dataset.}
  \label{fig:dataset_comparison}
\end{figure*}


The images we use are a subset of the corpus introduced in
\cite{lin2014microsoft}. The object annotations of these images were gathered by
\cite{visualgenome}. These objects were annotated as part of either a unary or
binary relationship. Unary relationships refer to the attributes associated with
each object (e.g. adjectival modifiers), while binary relationships capture the
information expressed by the occurrence of a pair of objects (e.g.
prepositional phrases). The result is a dataset of 5000 images with unary and
binary relationships of objects that are grounded in the image with bounding
boxes.

Our approach models the mapping between an image and a sentence using as a
Constrained State Problem (CSP) where each node corresponds to a object that
occurs in the sentence with its domain as a set of possible image objects that
could correspond to it. These mappings are not exact since the sentence might
refer to person in the image as a man while the image annotation might refer to
it as a human. To account for these discrepencies, we measure similarity using
a vectorized representation of the words describing the image
\cite{mikolov2013efficient}. Additionally, we model the attributes and binary
relationships between objects as unary and binary potentials in the CSP.

Finally, we create another model which integrates image features to learn a
probabilistic model for image retrieval. It models an image using a Conditional
Random Field (CRF) with each node corresponding to an object. The attributes of
each object model the unary potentials for its node. Finally, the pairwise
potentials model the edges between nodes corresponding to the binary
relationships that occur between objects in a scene. For each scene graphlet,
we extract attribute features like material and color for each object to
determine its unary potential. Likewise, we extract spatial features from
binary relationships to calculate the pairwise potential between nodes. Results
show that our approach can retrieve images that correspond to the semantic
meaning expressed by the scene graphlet. We show that this model significantly
improves the task of scene and sentence retrieval over our baseline.

\section{Related Work}
\label{related_work}
\paragraph{Datasets.} Datasets have long played a role in the development of new
computer vision algorithms. \cite{xiao2010sun} helped push forward the research
on scene classification while \cite{deng2009imagenet, russell2008labelme,
everingham2010pascal} helped build better classification and detection models.
To date, the largest computer vision datasets have been object-centric.
Finally, \cite{lin2014microsoft} is a non-iconic dataset with the aim of
understanding a scene through contextual resoning and 2D localization of
objects. The next step to scene understanding also involved learning about the
attributes of objects and relationships between objects.

\paragraph{Image Retrieval.} Many approaches for image retrieval have been
explored, including clustering \cite{ben2006improvingweb,
fritz2008decomposition}, graphical models \cite{fergus2005learning,
morsillo2009semi} and SVM classification \cite{schroff2011harvesting}. Other
retrieval tasks have focused on similarity measures using GIST features
\cite{oliva2001modeling}, attributes \cite{deng2011hierarchical,
farhadi2009describing} and hamming embeddings \cite{jegou2008hamming}.

\paragraph{Vectorized Word Representation.} \cite{collobert2008unified,
collobert2011natural} have shown that using vectorized representations of words
improve various NLP tasks.  \cite{mikolov2013efficient} introduced a skim-gram
model for converting words into vector spaces and evaluated its usefulness on
comparing similar words. Finally, \cite{mikolov2013distributed} showed that the
previous skim-gram model can be extended to even learn phrases such as ``Air
Canada'' instead of individually learning the words ``Air'' and ``Canada''. We
use these vectorized representations to compare nouns in the open vocabulary
used by our sentences and image annotations.

\paragraph{Structured Image Representation.} There has been much recent
interest in models that reason about images and natural language descriptions,
ranging from Flickr tags~\cite{li09cvpr} over sentences with canonical
structure~\cite{kulkarni2011baby,mitchell2012midge,zitnick2013learning} to
unaligned text
corpora~\cite{socher2010connecting,karpathyJF14,socher2013grounded}. These
models achieve impressive results in scene classification and object
recognition using only weak supervision. However, they are limited in terms of
expressiveness (e.g., ``baby talk''~\cite{kulkarni2011baby}). In contrast, our
scene graphlets are a structured representation of visual scenes. Each node is
explicitly grounded in an image region, avoiding the inherent referential
uncertainty of text-based representations.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.98\textwidth]{fruit_image.png}
  \caption{An example of the scene graphlet extracted from a given image. We
  display the groundings of scene graphlet from the objects in the image. Here
  we show each object as a label as well as a bounding boxed portion of the
  image. They are connected to each other through relationships.}
  \label{fig:fruit_image}
\end{figure*}


\paragraph{Bi-directional Mapping.} Quite a few papers have also explored the
possibilities of combining classification and detection with attributes and
relationships to communicate the contents of a scene. \cite{farhadi2010every}
extracts visual features and transforms an image into a \textit{meaning space}
in the form of triples connecting objects to retrieve sentences that correspond
to the scene. Similarly, \cite{ordonez2011im2text} extracts actions and uses a
similarity metric between images to understand them. \cite{li2011composing}
also generates sentences from scratch using n-grams of contextual information
from an image. Finally, \cite{karpathyJF14} creates a mapping between fragments
of a sentence to components of an image.

\section{Dataset}
\label{dataset}

Our dataset consists of 5000 images manually chosen from the intersection of
the Yahoo-Flickr Creative Commons 100M dataset and the Microsoft COCO
\cite{lin2014microsoft} dataset.

\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{9pt}
  \begin{tabular}{|l|r|}
  \hline
  Object Instances & 93,832 \\
  \hline
  Attribute Instances & 110,021 \\
  \hline
  Relationship Instances & 112,707 \\
  \hline
  \hline
  Objects Classes & 6,745 \\
  \hline
  Attribute Types & 3,743 \\
  \hline
  Relationship Types & 1,310 \\
  \hline
  \hline
  Object Per Image & 13.9 \\
  \hline
  Attribute Per Image & 29.4 \\
  \hline
  Relationships Per Image & 86.0 \\
  \hline
  \hline
  Attributes Per Object & 1.2 \\
  Relationsips Per Object & 2.4 \\
  \hline
  \end{tabular}
  \caption{Dataset Statistics}
\end{table}

We obtained object, attribute, and relationship annotations for these images
using crowd workers on Amazon Mechanical Turk. Workers are shown an image and
instructed to "write a short phrase about the image" such as "man wearing hat"
or "dog is brown". For each phrase, workers are also asked to draw bounding
boxes indicating the location in the image of the objects mentioned by the
phrase. Each image is seen by three workers, and each worker must write at
least 15 short phrases about each image. All worker-provided annotations
(objects, bounding boxes, short phrases) are independently verified by other
workers in separate tasks.

In total, our dataset consists of 93,832 annotated object instances, 115,020
instances of relationships between objects, and 112,945 object attributes.
There are 877 different categories of objects with at least 10 instances per
category, 128 types of relationships with at least 10 instances, and 514
attributes with at least 10 instances.

On average, we have around 18 image objects per image, along with 22 unary
attributes and 22 binary relationships. See Figure~\ref{fig:dataset_comparison}
for the distribution of annotations.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.98\textwidth]{sentence_parsing.png}
  \caption{Given a query sentence (a), we will first extract its typed
  dependency (b) and then convert it into a scene graphlet (c).}
  \label{fig:sentence_parsing}
\end{figure*}

\section{Graphlet Only Model}
Our strategy is to convert both images and sentences into scene graphlets and
use those graphlets as queries to retrieve similar images and sentences. To do
this, we must be able to measure the agreement between a query scene graphlet
and an unannotated test image. We assume that this agreement can be measured by
examining the best possible grounding of the scene graphlet to the test image.

Intuitively, a sentence that describes the graph or a graph that illustrates
the sentence should have similar contents. A naive way to determine if a
sentence matches a graph is to see if the contents in the sentence have a one
to one correspondence with the contents in the graph. Since the nature of the
task is an assignment problem, we form a CSP model that will return the
assignment of sentences to images (and vise versa) ranked by descending CSP
assignment weight.

\subsection{CSP Formulation}
The variables and domains for our CSP model are the objects (nouns) in the
sentence and their domains are the image annotation objects. Consider an image
annotated with objects $O_{i} = \{a_1, a_2, ... , a_n\}$ and a sentence
annotated with objects $O_{s} = \{b_1, b_2, ... , b_m\}$.  For image retrieval,
we will make $O_{s}$ the variables of CSP and $O_{i}$ the domain for each
variable in $O_{s}$.

In the case where the domain size is smaller than the number of variables, we
will pad empty string into the domain so that domain size is always at least as
large as the number of variables to ensure possible assignment.  Naturally, to
prevent the CSP from always assigning an empty string to a variable, we also
penalize it for every empty string.

The CSP problem will find us the best assignment of each object in $O_{s}$ to
variables in $O_{i}$ and give us the weight for the corresponding best
assignment. Let $A = \{(a_1, b_i), ... , (a_n, b_j)\}, i, j \in \{1,2, ...,
m\}$ be one possible assignment. We impose both unary and binary potentials of
the graph as following:

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{CSP.jpg}
  \caption{Example CSP graph where the objects in the sentence are ``car'',
  ``dog'', etc. while it's domain is extracted from ``bus'', ``cat'', etc.}
  \label{fig:CSP}
\end{figure}

\paragraph{Unary potentials.} The unary potential for a variable $a_i$ and an
assignment $b_j$ is the word similarity between the name of variable (which is
the word for the corresponding object) and word for object $b_j$. We convert
the two words into vector using \textit{word2vec} and compute the cosine
similarity between the resulting vectors. Thus, the unary potential is:
\begin{equation}
f_u(a_i, b_j) = cos(word2vec(a_i), word2vec(b_j))
\end{equation}

\paragraph{Binary potentials.} The binary constraint for our model ensures that
no two variables can be assigned to the same value in the domain. We assign a
binary potential between every single pair of variables to ensure that they all
have a unique object assigned.
\begin{equation}
f_b(a_i, a_j) = [\text{assignments of } a_i \text{ and } a_j \text{ are different }]
\end{equation}

\paragraph{Attributes.} To encode attributes in the CSP, we create additional
unary potentials, one for every attribute that the object assossiated with the
variable has. For example, if man has 2 attributes: tall and thin, the variable
associated with man will also have 2 unary potentials. This allows the model to
encourage matching of objects between images and sentences if they have the
same attribute.

\paragraph{Binary Relationships.} Finally, we model binary relationships
between pairs of objects intuitive as binary potentials between those
particular pairs of objects.  For example, if a ``bus'' is on a ``street'',
there will be a binary potential between the ``bus'' and the ``street''.
\paragraph{Weight of assignment.} The weight of assignment $A$ is
\begin{equation}
W = \Pi_{i, i \in {1, ... n}} f_b(a_i, a_j) * \Pi_{(var, val) \in A} f_u(var, val)
\end{equation}
Using the CSP model described above, each image and sentence pair will have a
score corresponding to their best CSP assignment. We then sort the sentences
for each image by score to give a rank of image retrieval. Similarly, we can
generate a rank of images for each sentence retrieval.

\subsection{Implementation Detail}
We do not penalize assignments of variables if the attribute or the binary
relationships that correspond to them are not met. In numerous cases, the
sentences describing an image are very minimal, capturing only the salient
object and maybe one or two attributes or relationships.  Meanwhile, the
annotations in the images are very comprehensive and contain a lot of
attributes.  To prevent our model from predicting incorrectly on images that
contain more such relationships than others, we decided not to penalize.

\section{Graphlet With Image Features Model}
\label{model}
We also wanted to check the influence of visual image features in image retrieval.
To this end, we construct a conditional random field (CRF~\cite{lafferty01icml})
that models the distribution over all possible groundings. We perform maximum
a posteriori (MAP) inference to find the most likely grounding; the likelihood
of this MAP solution is taken as the score measuring the agreement between the
scene graphlet and the image.

\subsection{CRF Formulation}

Let $G=(O,E)$ be a scene graphlet, $B$ be a set of bounding boxes in an image, and
$\gamma$ a grounding of the scene graphlet to the image. Each object $o\in O$
gives rise to a variable $\gamma_o$ in the CRF, where the domain of $\gamma_o$
is $B$ and setting $\gamma_o=b\in B$ corresponds to grounding object $o$ in the
scene graphlet to box $b$ in the image. We model the distribution over possible
groundings as
{\small
\begin{equation}
  P(\gamma \mid G, B) = \prod_{o\in O}P(\gamma_o\mid o)
  \hspace{-4mm} \prod_{(o,r,o')\in E} \hspace{-4mm} P(\gamma_o, \gamma_{o'}\mid o,r,o').
\end{equation}
}

Using Bayes' rule, we may rewrite $P(\gamma_o\mid o)$ as $P(o\mid\gamma_o)
P(\gamma_o) / P(o)$. Assuming uniform priors over bounding boxes and object
classes, $P(\gamma_o)$ and $P(o)$ are constants and can be ignored when
performing MAP inference. Therefore, our final objective has the form
 \begin{equation}
   P(\alpha_o\mid o) = \frac{1}{P(o)} P(o\mid\alpha_o) P(\alpha_o)
 \end{equation}
We assume uniform priors over bounding boxes and object classes, so
$P(\alpha_o)$ and $P(o)$ are constants and can be ignored when performing MAP
inference. Therefore our objective has the form
{\small
\begin{equation}
  \label{eq:objective}
  \gamma^* = \arg\max_\gamma \prod_{o\in O}P(o\mid\gamma_o)
  \hspace{-3.5mm} \prod_{(o,r,o')\in E} \hspace{-3.5mm} P(\gamma_o,\gamma_{o'}\mid o,r,o').
\end{equation}
}

\vspace{-8mm}

\paragraph{Unary potentials.}
The term $P(o\mid \gamma_o)$ in Equation~\ref{eq:objective} is a unary
potential modeling how well the appearance of the box $\gamma_o$ agrees with
the known object class and attributes of the object $o$. If $o=(c, A)$, then we
decompose this term as
{\small
\begin{equation}
  P(o\mid\gamma_o) = P(c\mid\gamma_o)\prod_{a\in A}P(a\mid \gamma_o).
\end{equation}
}
The terms $P(c\mid \gamma_o)$ and $P(a\mid\gamma_o)$ are simply the
probabilites that the bounding box $\gamma_o$ shows object class $c$ and
attribute $a$. To model these probabilities, we use R-CNN \cite{girshick14CVPR}
to to train detectors for each of the $|\mathcal{C}|=266$ and
$|\mathcal{A}|=145$ object classes and attribute types. We apply Platt
scaling~\cite{platt99lmc} to convert the SVM classification scores for each
object class and attribute into probabilities.

\paragraph{Binary potentials.}
The term $P(\gamma_o,\gamma_{o'}\mid o,r,o')$ in Equation~\ref{eq:objective} is
a binary potential that models how well the pair of bounding boxes
$\gamma_o,\gamma_{o'}$ express the tuple $(o, r, o')$. Let $\gamma_o=(x, y, w,
h)$ and $\gamma_{o'}=(x',y',w',h')$ be the coordinates of the bounding boxes in
the image. We extract features $f(\gamma_o, \gamma_{o'})$ encoding their
relative position and scale:
{\small
\begin{equation}
  f(\gamma_o, \gamma_{o'}) = \left(\frac{x-x'}{w}, \frac{y-y'}{h}, \frac{w'}{w},
                                   \frac{h'}{h}\right)
\end{equation}
}
Suppose that the objects $o$ and $o'$ have classes $c$ and $c'$, respectively.
Using the training data, we train a Gaussian mixture model (GMM) to model
$P(f(\gamma_o,\gamma_{o'}) \mid c, r, c')$. If there are fewer than 30
instances of the tuple $(c, r, c')$ in the training data then we instead fall
back on an object agnostic model $P(f(\gamma_o,\gamma_{o'})\mid r)$. In either
case, we use Platt scaling to convert the value of the GMM density function
evaluted at $f(\gamma_o,\gamma_{o'})$ to a probability
$P(\gamma_o,\gamma_{o'}\mid o,r,o')$.

\subsection{Implementation Details}
We compared the performance of several methods for generating candidate boxes
for images, including Objectness~\cite{alexe2012measuring}, Selective search
(SS~\cite{UijlingsIJCV2013}), and Geodesic Object Proposals
(GOP~\cite{krahenbuhl2014geodesic}). We use GOP for all experiments. We perform
approximate inference using off-the-shelf belief
propagation~\cite{andres2012opengm}.

\begin{table*}[t]
  \centering
  \setlength{\tabcolsep}{5pt}
  \begin{tabular}{|c|c|c|c|c|}
  \hline
  Model & Recall @ 1 & Recall @ 5 & Recall @ 10 & Median Rank \\
  \hline
  \hline
  Baseline & 0.0 & 0.0 & 0.2 & 460 \\
  \hline
  BRNN (Oracle) & 8.9 & 24.9 & 36.9 & 12.4 \\
  \hline
  \hline
  Greedy (objects only) & 0.0 & 0.0 & 4.6 & 55 \\
  \hline
  CSP (objects only) & 1.6 & 5.8 & 9.7 & 31 \\
  \hline
  CSP (full model) & 1.8 & 6.6 & 13.6 & 28 \\
  \hline
  CRF (object only) & 1.4 & 5.9 & 12.2 & 30 \\
  \hline
  CRF (full model) & 3.9 & 10.3 & 18.5 & 25 \\
  \hline
  \end{tabular}
  \caption{Results after evaluating on a test set of 1000 images on a model
  trained using 4000 images.}
  \label{table:results}
\end{table*}

\section {Evaluation}
\label{evaluation}
We conduct two main experiments using our data, concentrating on the task of
image retrieval. The first test is checking the importance of relationships
between and attributes of objects as factors in retrieval. To this end, we
build models that use only the objects in the sentence and image graphlets and
compare them against the models that use attributes as well as relationships.
The models listed in Table~\ref{table:results} are suffixed with (objects only)
and (full model) to distinguish between a model that uses only objects and one
that also uses attributes and relationships.

The second experiment we run is the use of image features into our model. To
this end, we use a CSP that only compares graphlets and a CRF model that
incorporates bounding box information as well.

\subsection{Baseline}
\label{baseline}
\textbf{Random} For our first baseline model, we use a random ranking algorithm
that ignores the given sentence or image query and returns a random ranking of
all the images in the test for image retrieval and a random ranking of all the
sentences in the test set for sentence retrieval.

\subsection{Oracle}
\label{oracle}
For our Oracle, we used the image retrieval model introduced by
\cite{karpathyJF14}.  It uses a deep learning framework where the layers
capture relative information between adjacent words in a sentence to learn a
mapping from the words to objects in an image. It uses the COCO dataset as
well. It also performs very well with a median rank of $12.4$ and a $8.9\%$
Recall at 1.

\subsection{Greedy model}
We use Stanford Parser to convert the sentence into a typed dependency list as
shown in Figure~\ref{fig:sentence_parsing}. Next, we extract the objects from
the list and use their vectorized representations to match them against the
scene graphlets of our test images. We return the images in our test set ranked
according to best match based on just the objects.

We use a greedy algorithm where we iterate over all the objects in the sentence
graphlet and just assign it to the object in the image graphlet that it is most
similar too. The similarity of two objects is measured by the cosine
similarities of their vectorized representations.

The model performs reasonly well with a $4.6\%$ recall at the top 10 images
retrieved while achieving a median rank of 55, which is a huge improvement from
the baseline's median rank of $460$.

\subsection{CSP}
Unlike the Greedy Model, which achieves a local minima, the CSP model manages
to find the best assignment of objects between the two graphlets and is able to
outperform the greedy model at a huge cost to runtime efficiency. It manages to
achieve a median rank of 31 while the recall at 10 also improves to $9.7\%$.

We notice further improvement when using the attributes and relationships as
unary and binary potentials as described earlier. The reason for the
improvements are explained later in the analysis section. The median rank
increases to 28 while the recall increases by $0.2\%$, $0.8\%$ and $3.9\%$ at
1, 5 and 10, respectively.

\subsection{CRF}
The CRF model uses the image features of the image as well but unfortunately is
not able to generalize well and recall at 1 drops by $0.2\%$ from the CSP
(object only) model. Furthermore, we see an improvement with the inclusion of
attributes and relationships in our CRF (full model) which is able to improve
the median rank to 25 while increasing the recall by $2.1\%$, $3.9\%$ and
$6.3\%$ against the CSP (full model).

\section{Results Analysis}
In this section, we will go into some of the qualitative results that we found
particularly interesting.

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{relationships.png}
  \caption{Qualitative results obtained from the (a) just image features, (b)
  using a model that only incorporates objects, (c) model with attributes, and
  (d) model with relationships.}
  \label{fig:relationships}
\end{figure*}

\subsection{Relationships}
Figure~\ref{fig:relationships} shows examples for the top results returned from
the query: ``A man riding a brown horse''.  The results returned by only using
image features returns absolutely no correct results in the top 5. Due to the
large number of GIST proposals generated for every image, it is really
difficult in assigning them to a sentence graphlet.

Using the object model, however, does manage to return some useful results.
Unfortunately, it failed to distinguish an elephant from a horse and has no
notion of whether the person is riding the horse or not.

Adding attributes in Figure~\ref{fig:relationships}(b) improves the results
further by making sure that only brown colored animals are returned. And
finally, adding relationships allow us to capture the action between the man
and the horse.

\subsection{Graphlet Alignment}
Incorporating relationships into our model caused the increase in recall values and
improvements in our ranking because it improved the alignments of graphlets between
sentence and images.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.95 \linewidth]{alignment.png}
   \caption{Example where the using only objects leads to incorrect alignment
   of ``man'' and ``woman'' objects, while the model that incorporates
   relationships manages to align the objects correctly.}
\label{fig:alignment}
\end{figure}

In the example shown in Figure~\ref{fig:alignment}, we wanted to check the
alignment or matching the the sentence objects to the image objects using the
CSP model. The objects in the sentence were ``man'', ``woman'' and ``umbrella''
and contained the relationships: ``woman holding umbrella'' and ``man next to
woman''. Using just the objects and ignoring the attributes and relationships,
created an incorrect alignment in the CRF model resulted in an incorrect
alignment of the objects, since both ``man'' and ``woman'' are close in visual
feature space.

However, once we included the binary potential associated with ``woman holding
umbrella'', the model that incorporated the relationships was easily able to
correct assign woman to the object closest and directly below the umbrella.
This accounts for one of the reasons why a model with relationships performs
better than the object only model.

\section{Error Analysis}
\label{error_analysis}

\subsection{Sentence Parsing}

One main cause for our errors came from the limitations of the sentence parser.
The parser was often unable to determine the correct grammatical structure of
the sentences. The fact that most of the sentences were not strictly
complete sentences with a subject and verb made the problem more difficult. As
a consequence, the parser was unable to determine many of the dependency
relations between words and labeled them with the default ``dependency''
instead of something more specific such as ``verbal modifier''. Without a more
specific label, it was difficult to determine whether a dependency relation
should be extracted as a unary/binary relationship or not.

\begin{figure*}[t]
   \centering
   \includegraphics[width=0.95 \linewidth]{sentence_error.png}
   \caption{An example of the limitations of the Stanford Parser when
   constructing sentence graphlets.}
\label{fig:sentence_error}
\end{figure*}

However, the bigger problem was the ambiguity of the English language. For
example, ``brick'' can be either a noun or an adjective depending on its
context. When parsing ``A red brick school building\dots'', the Stanford Parser
incorrectly identifies ``brick'' as a noun and extracts ``brick school'' as a
single compound noun or entity, instead of extracting ``school'' as the entity
and ``brick'' as an attribute of ``school''. This means that queries for ``red
school'' do not match well against this sentence, because ``school'' and
``brick school'' are two different entities.

Another common mistake due to language ambiguity occurred in prepositional
phrases. In the example sentence from Figure ~\ref{fig:sentence_error}, ``A
bakery with\dots donuts in the display case in front of customers,'' the
Stanford Parser suggests that the \textit{bakery} is in the display case and
that the \textit{bakery} is in front of the customers; from the context, humans
can easily determine that the \textit{donuts} are in the display case and the
\textit{display case} is in front of the customers.  This error completely
changes the meaning of the sentence and the images that might be associated
with it.

\subsection{Word Comparison}
Another area of error came from using word2vec to measure the similarity of
different words. Although word2vec is good at grouping together similar words
such as ``woman'', ``women'', and ``female''. Potentially, these errors can
avoided by using a lower threshold when comparing plural to singular forms of 
a word. However, by lowering the threshold, we also invariably introduce the 
chance of error amongst words that are clost in vector space but far in semantics.

A larger problem still is word2vec's tendency to group together
words from the same family. For example, ``red'' and ``yellow'' are both
colors, so word2vec maps them to similar vectors. Although they are close in
the word2vec space, visually, red and yellow are very different.

\subsection{Discrepancies between Sentences and Annotations}.
There are multiple instance of objects that are mentioned in sentences but do
not exist in our annotations, often as a result of the parsing errors mentioned
above. For example, we have 484 instances of ``city street'' but no annotations
of it. We do, however, have many ``streets'' annotated. Unfortunately, these
are not matched with each other and result in lower ranks of the correct
images.

Instances such as ``baseball player'' being annoted as ``person'' in some
images causes many ground truth images to not properly fit.

Additional high level concepts such as ``living room'' and ``baseball game''
have 90 and 62 instances that are not annotated, as you can not ground an entire
image as being a living room or a baseball game.

\subsection{Similarity in Visual Features}
There are examples of objects that have very similar visual features. For
example, both ``man'' and ``woman'' are very similar. Unfortunately, our models
can not differentiate between them and we get images of ``women'' when querying
for a ``man'' and vice versa.

Also, we have annotations of ``person'' and ``man'' that we have not
canonicalized.  These lead to learning separate models for both these classes
of objects. And when we query for a ``person'', we are not returned any
instances of ``man'' or ``boy'' etc.

\subsection{Long Tail and Sparcity}
Finally, the last type of errors that we see are instances of queries that have
really low median rank scores. On further inspection, we discovered a long tail
of objects that occurred infrequently, many occurring only once in the entire
dataset. Some examples of these objects are ``fireplace'', ``canoe'',
``nintendo'' and ``gazebo''. Out of the 93,000 object instances that we have,
about 30,000 of them occur less than 5 times in the entire dataset.

\section{Future Work}
There are numerous ways in which this project can be improved and extended.
One clear example is turning the tables around and conducting sentence
retrieval instead of image retrieval and comparing the two results. Since the
representations of the two forms of data are in the form of identical
graphlets, they can be easily switched. Unfortunately, we will also have to
create the CSP's with the objects in the image as variables. Since each image
has an average of 22 objects while sentences have about 2 objects, the CSPs
would invariably contain more variables and would drastically increase the
runtime.

We can also reduce a lot of errors by benchmarking and avoiding some of the
common errors that we have identified in sentence parsing, vectorization, and
sparcity. Additionally, by avoiding uncommon or non-occuring words, we can
improve our error rates. An even better solution would be to canonicalize the
data to a hierarchial dataset such as Freebase \cite{bollacker2008freebase}.

Finally, if we had more data, we could also use a deep learning approach and
learn hidden representations between images and sentences along with the
grounded annotations.

\section{Conclusion}
In this paper, we have introduced the concept of graphlets, which can be used
to encode the semantic meaning of images. Additionally, we have shown how these
graphlets can be used to encode the semantic meaning of sentences as well,
which naturally makes the comparison between images and sentences possible. We
have especially argued the importance of these graphlets by using them in the
task of image retrieval. Moreover, with regards to image retrieval, we have
demonstrated that models that only incorporate objects are not sufficient to
fully understand images, and that incorporating relationships between objects
is important in improving the ranking task. Finally, we show that incorporating
image features improves retrieval even further, but introduces a whole new set
of challenges and errors.

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
\nocite{*}
}

\end{document}
